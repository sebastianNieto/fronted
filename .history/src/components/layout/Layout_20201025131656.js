import React from 'react';
import { Layout } from 'antd';
const { Header, Footer, Content } = Layout;
import Nav from './Nav';

export default function LayoutContainer({children}) {
  return (
    <Layout>
      <Header>
        h
      </Header>
      <Content>
        {children}
      </Content>
      <Footer>Footer</Footer>
    </Layout>
  )
}
