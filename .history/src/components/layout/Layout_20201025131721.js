import React from 'react';
import { Layout } from 'antd';
const { Header, Footer, Content } = Layout;


export default function LayoutContainer({children}) {
  return (
    <Layout>
      
      <Content>
        {children}
      </Content>
      <Footer>Footer</Footer>
    </Layout>
  )
}
