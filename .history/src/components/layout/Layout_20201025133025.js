import React from 'react';
import { Layout } from 'antd';
import Nav from './Nav';
const { Header, Footer, Content } = Layout;
import './styles.css'

export default function LayoutContainer({children}) {
  return (
    <Layout>
      <Header className="nav">
        <Nav />
      </Header>
      <Content>
        {children}
      </Content>
      <Footer>Footer</Footer>
    </Layout>
  )
}
