import React from 'react';
import { Layout } from 'antd';
import Nav from './Nav';
import './styles.css';
const { Header, Footer, Content } = Layout;

export default function LayoutContainer({children}) {
  return (
    <Layout className="container">
      <Header className="nav">
        <Nav />
      </Header>
      <Content>
        {children}
      </Content>
      <Footer>Footer</Footer>
    </Layout>
  )
}
