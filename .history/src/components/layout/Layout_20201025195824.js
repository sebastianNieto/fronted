import React from 'react';
import Context from '../../context/Context';
import { navigate } from "@reach/router"
import { Layout } from 'antd';
import Nav from './Nav';
import './styles.css';
const { Header, Footer, Content } = Layout;

export default function LayoutContainer({children}) {
  const { stateLogin, dispatchLogin } = React.useContext(Context.Context);
  const closeSesion = () => {
    dispatchLogin({
      type: 'LOGOUT',
      payload: {
        isLogin: false
      },
    })
    navigate('/login')
  }

  return stateLogin.isLogin ? (
    <Layout className="container-layout">
      <Header className="nav">
        <Nav />
      </Header>
      <Content style={{flex: 1}}>
        {children}
      </Content>
      <Footer ><a onClick={closeSesion}>Cerrar sesión</a></Footer>
    </Layout>
  ): null
}
