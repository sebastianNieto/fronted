import React from 'react';
import { Menu } from 'antd';
import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import './styles.css';

const { SubMenu } = Menu;

export default function Nav() {

  const handleClick = e => {
    console.log('click ', e);
  };

  return (
    <Menu onClick={handleClick} className="nav"  mode="horizontal">
      <SubMenu key="SubMenuProduct" icon={<SettingOutlined />} title="Productos">
        <Menu.ItemGroup title="">
          <Menu.Item key="setting:1">Crear</Menu.Item>
          <Menu.Item key="setting:2">Listar</Menu.Item>
        </Menu.ItemGroup>
      </SubMenu>
      <SubMenu key="SubMenuCategory" icon={<SettingOutlined />} title="Categorías">
        <Menu.ItemGroup title="">
          <Menu.Item key="setting:1">Crear</Menu.Item>
          <Menu.Item key="setting:2">Listar</Menu.Item>
        </Menu.ItemGroup>
      </SubMenu>
    </Menu>
  )
}
