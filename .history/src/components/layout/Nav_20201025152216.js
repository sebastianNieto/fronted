import React from 'react';
import { Menu } from 'antd';
import { navigate } from "@reach/router"
import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;

export default function Nav() {

  const handleClick = e => {
    navigate(e.key);
  };

  return (
    <Menu onClick={handleClick}   mode="horizontal">
      <SubMenu key="SubMenuProduct" icon={<SettingOutlined />} title="Productos">
        <Menu.ItemGroup title="">
          <Menu.Item key="crearProducto">Crear</Menu.Item>
          <Menu.Item key="ListProduct">Listar</Menu.Item>
        </Menu.ItemGroup>
      </SubMenu>
      <SubMenu key="SubMenuCategory" icon={<SettingOutlined />} title="Categorías">
        <Menu.ItemGroup title="">
          <Menu.Item key="Category">Crear</Menu.Item>
          <Menu.Item key="ListCategory">Listar</Menu.Item>
        </Menu.ItemGroup>
      </SubMenu>
    </Menu>
  )
}
