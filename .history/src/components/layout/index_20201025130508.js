import React from 'react';
import { Layout } from 'antd';
const { Header, Footer, Sider, Content } = Layout;
import Nav from './Nav';

export default function Layout({children}) {
  return (
    <Layout>
      <Header>
        <Nav />
      </Header>
      <Content>
        {children}
      </Content>
      <Footer>Footer</Footer>
    </Layout>
  )
}
