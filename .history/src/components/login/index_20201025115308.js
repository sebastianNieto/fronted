import React from 'react';
import { Form, Input, Button, Typography } from 'antd';
import './styles.css';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 8 },
};

export default function Login({ onFinish, onFinishFailed }) {
  const { Title } = Typography;
  return (
    <div className="container">
      <Title type="primary" level={2}>Iniciar Sesión</Title>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="user"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Autenticar
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
