import React from 'react'
import { Form, Input, InputNumber, Button, Typography, Select, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import './styles.css'

const { Option } = Select;
const layout = {
  labelCol: { offset: 8, span: 8 },
  wrapperCol: { offset: 8, span: 8 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 8 },
};


export default function CrearProducto({onFinish, onFinishFailed, onHandleChangeImage, beforeUpload}}) {
  const { Title } = Typography;
  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );
  return (
    <div className="container-producto">
      <Title type="primary" level={2}>Crear un nuevo producto</Title>
      <Upload
        name="avatar"
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        beforeUpload={beforeUpload}
        onChange={onHandleChangeImage}
      >
        {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
      </Upload>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        layout="vertical"
      >
        <Form.Item
          label="Nombre Producto"
          name="nombre"
          rules={[{ required: true, message: 'Falta ingresar el nombre del producto' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Descripción del Producto"
          name="descripcion"
          rules={[{ required: true, message: 'Falta ingresar la descripción del producto' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Precio"
          name="precio"
          rules={[{ required: true, message: 'Falta ingresar el nombre del producto' }]}
        >
          <InputNumber style={{ width: '100%' }} />
        </Form.Item>

        <Form.Item
          label="Categoria"
          name="password"
          rules={[{ required: true, message: 'Seleccione una categoria' }]}
        >
          <Select defaultValue="lucy" style={{ width: '100%'}} onChange={(a) => console.log(a)} >
            <Option value="jack">Jack</Option>
            <Option value="lucy">Lucy</Option>
            <Option value="Yiminghe">yiminghe</Option>
          </Select>
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Crear
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
