import React from 'react';
import { Row, Col, Divider, Card } from 'antd';

export default function ListarProductos({products}) {
  return (
    <div>
      {
        products.map((product) => {
          return (
            <Card title="Default size card" extra={<a href="#">Delete</a>} style={{ width: 300 }}>
              {product.name}
            </Card>
          )
        })
      }
      
    </div>
  )
}
