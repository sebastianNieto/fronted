import React from 'react';
import { Row, Col, Divider, Card } from 'antd';
import {apiruta} from '../../utilities/constants';

export default function ListarProductos({products}) {
  return (
    <div style={{display: "flex"}}>
      {
        products.map((product) => {
          return (
            <Card title="Default size card" extra={<a href="#">Delete</a>} style={{ width: 300 }}>
              <img src={apiruta + `/uploads/products/${product.image}`} alt=""/>
              {product.name}
            </Card>
          )
        })
      }
      
    </div>
  )
}
