import React from 'react';
import { Row, Col, Divider, Card, Alert } from 'antd';
import {apiruta} from '../../utilities/constants';

export default function ListarProductos({products}) {
  return (
    <div className="container-card">
      {
        products.map((product) => {
          return (
            <Card title="Default size card" extra={<a href="#">Delete</a>} style={{ width: 300 }}>
              <img src={apiruta + `/uploads/products/${product.image}`} alt=""/>
              <Alert message={product.name} type="info" />
            </Card>
          )
        })
      }
      
    </div>
  )
}
