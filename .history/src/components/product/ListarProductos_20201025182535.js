import React from 'react';
import { Row, Col, Divider, Card, Alert } from 'antd';
import {apiruta} from '../../utilities/constants';

export default function ListarProductos({products, onDelete, handleOk, handleCancel, confirmLoading}) {
  return (
    <div className="container-card" style={{marginTop: '20px'}}>
      {
        products.map((product) => {
          return (
            <Card title="Default size card" extra={<a onClick={(e) => onDelete(e, product.id)} href="#">Delete</a>} style={{ width: 300, textAlign: 'center' }}>
              <img src={apiruta + `/uploads/products/${product.image}`} alt=""/>
              <Alert message={product.name} type="info" />
            </Card>
          )
        })
      }
      <div>
        <Button type="primary" onClick={this.showModal}>
          Open Modal with async logic
        </Button>
        <Modal
          title="Title"
          visible={visible}
          onOk={this.handleOk}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
        >
          <p>{ModalText}</p>
        </Modal>
      </div>
    </div>
  )
}
