import React from 'react';
import { Row, Col, Divider, Card, Alert, Modal } from 'antd';
import {apiruta} from '../../utilities/constants';

export default function ListarProductos({ products, onDelete, handleOk, handleCancel, confirmLoading, modalText, visible, showModal, productInfo}) {
  return (
    <div className="container-card" style={{marginTop: '20px'}}>
      {
        products.map((product) => {
          return (
            <Card title="Default size card" extra={<a onClick={(e) => onDelete(e, product.id)} href="#">Delete</a>} style={{ width: 300, textAlign: 'center' }}>
              <img src={apiruta + `/uploads/products/${product.image}`} alt="" onClick={(e) => showModal(e, product.id)} />
              <Alert message={product.name} type="info" />
            </Card>
          )
        })
      }
      <div>
        <Modal
          title="Title"
          visible={visible}
          onOk={handleOk}
          confirmLoading={confirmLoading}
          onCancel={handleCancel}
        >
          <img src={apiruta + `/uploads/products/${productInfo.image}`} alt={`${productInfo.name}`}/>
          <div>{`${productInfo.name}`}</div>
          <div>{`${productInfo.description}`}</div>
          <div>{`${productInfo.price}`}</div>
        </Modal>
      </div>
    </div>
  )
}
