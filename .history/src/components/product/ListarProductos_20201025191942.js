import React from 'react';
import { Card, Alert, Modal, InputNumber, Button, Form } from 'antd';
import {apiruta} from '../../utilities/constants';

export default function ListarProductos({ products, onDelete, handleOk, handleCancel, confirmLoading, modalText, visible, showModal, productInfo, onFinish, onFinishFailed}) {
  return (
    <div className="container-card" style={{marginTop: '20px'}}>
      {
        products.map((product) => {
          return (
            <Card title={`${product.name}`} extra={<a onClick={(e) => onDelete(e, product.id)} href="#">Delete</a>} style={{ width: 300, textAlign: 'center' }}>
              <img className="image-card" src={apiruta + `/uploads/products/${product.image}`} alt="" onClick={(e) => showModal(e, product.id)} />
              <Alert message={product.price} type="info" />
              <Form
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
              >
                <Form.Item name="cantidad">
                  <InputNumber defaultValue={1} min={1} />
                </Form.Item>
                <Button type="primary" htmlType="submit">Agregar al carrito</Button>
              </Form>
            </Card>
          )
        })
      }
      <div>
        <Modal
          title={`${productInfo.name}`}
          visible={visible}
          onOk={handleOk}
          confirmLoading={confirmLoading}
          onCancel={handleCancel}
          style={{textAlign: "center"}}
        >
          <img className="image-card" src={apiruta + `/uploads/products/${productInfo.image}`} alt={`${productInfo.name}`}/>
          <div>Nombre: {`${productInfo.name}`}</div>
          <div>Descripción: {`${productInfo.description}`}</div>
          <div>Precio: {`${productInfo.price}`}</div>
          <div>Fecha: {`${productInfo.date_creation}`}</div>
        </Modal>
      </div>
    </div>
  )
}
