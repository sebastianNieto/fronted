import React from 'react';
import LoginLayout from '../components/Login';

export default function Login() {
  const onFinish = values => {
    console.log('Success:', values);
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  return (
    <LoginLayout
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    />
  )
}
