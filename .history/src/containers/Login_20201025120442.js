import React from 'react';
import { apiruta } from '../utilities/constants';
import LoginLayout from '../components/login/index';

export default function Login() {
  const onFinish = async values => {
    const response = await fetch( apiruta + 'Login/login', {
      headers: {
        'Content-Type': 'application/json'
      },
    });

    const responseJson = await response.json();
    console.log(responseJson)
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  return (
    <LoginLayout
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    />
  )
}
