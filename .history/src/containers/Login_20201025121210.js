import React from 'react';
import Context from '../context/Context';
import { apiruta } from '../utilities/constants';
import LoginLayout from '../components/login/index';
import { redirectTo } from "@reach/router"

export default function Login() {
  const { stateLogin, dispatchLogin } = React.useContext(Context.Context);
  const onFinish = async values => {
    try {
      const response = await fetch(apiruta + 'Login/login', {
      });
      const responseJson = await response.json();
      if(responseJson.status === 1) {
        await dispatchLogin({
          type: 'LOGIN',
          payload: {
            user: responseJson.data.dataUser.user,
            name: responseJson.data.dataUser.name,
            token: responseJson.data.token,
            isLogin: true
          },
        })
        redirectTo("/home")
      }
    } catch (error) {
      
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  return (
    <LoginLayout
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    />
  )
}
