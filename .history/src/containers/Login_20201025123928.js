import React from 'react';
import Context from '../context/Context';
import { apiruta } from '../utilities/constants';
import { createFormData } from '../utilities/functions';
import LoginLayout from '../components/login/index';
import { Redirect, redirectTo } from "@reach/router"

export default function Login() {
  const { dispatchLogin } = React.useContext(Context.Context);
  const [isLogin, setIsLogin] = React.useState(false);
  const onFinish = async values => {
    try {
      const formulario = createFormData(values)
      const response = await fetch(apiruta + 'Login/login', {
        method: 'post',
        body: formulario
      });
      const responseJson = await response.json();
      if(responseJson.status === 1) {
        // dispatchLogin({
        //   type: 'LOGIN',
        //   payload: {
        //     user: responseJson.data.dataUser.user,
        //     name: responseJson.data.dataUser.name,
        //     token: responseJson.data.token,
        //     isLogin: true
        //   },
        // })
        setIsLogin(true);
      }
    } catch (error) {
      console.log(error)
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  if(isLogin) {
    redirectTo("/login")
  }
  return (
    <LoginLayout
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    />
  )
}
