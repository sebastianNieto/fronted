import React from 'react';
import Context from '../context/Context';
import { apiruta } from '../utilities/constants';
import { createFormData } from '../utilities/functions';
import LoginLayout from '../components/login/index';
import { navigate } from "@reach/router"
import { message } from 'antd';

export default function Login() {
  const { dispatchLogin } = React.useContext(Context.Context);
  const onFinish = async values => {
    try {
      const formulario = createFormData(values)
      const response = await fetch(apiruta + 'Login/login', {
        method: 'post',
        body: formulario
      });
      const responseJson = await response.json();
      if(responseJson.status === 1) {
        dispatchLogin({
          type: 'LOGIN',
          payload: {
            user: responseJson.data.dataUser.user,
            name: responseJson.data.dataUser.name,
            token: responseJson.data.token,
            isLogin: true
          },
        })
        navigate('/')
      }
      else {
        message.error(responseJson.data, 5);
      }
    } catch (error) {
      message.error(error, 5);
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <LoginLayout
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    />
  )
}
