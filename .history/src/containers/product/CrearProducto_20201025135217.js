import React from 'react';
import { message } from 'antd';
import { apiruta } from '../../utilities/constants';
import { createFormData } from '../../utilities/functions';
import CrearProductoLayout from '../../components/product/CrearProducto';

export default function CrearProducto() {
  const onFinish = async values => {
    try {
      const formulario = createFormData(values)
      const response = await fetch(apiruta + 'Login/login', {
        method: 'post',
        body: formulario
      });
      const responseJson = await response.json();
      if (responseJson.status === 1) {

        
      }
      else {
        message.error(responseJson.data, 5);
      }
    } catch (error) {
      message.error(error, 5);
    }
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  return (
    <CrearProductoLayout 
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    />
  )
}
