import React from 'react';
import Context from '../../context/Context';
import { message } from 'antd';
import { apiruta } from '../../utilities/constants';
import { createFormData } from '../../utilities/functions';
import CrearProductoLayout from '../../components/product/CrearProducto';

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!', 5);
  }
  return isJpgOrPng && isLt2M;
}

export default function CrearProducto() {
  const { stateLogin } = React.useContext(Context.Context);
  const [loading, setLoading]  = React.useState();
  const [imageUrl, setImageUrl] = React.useState('');
  const [categories, setCategories] = React.useState([]);

  React.useEffect(() => {
    getCategories()
  }, []);

  const getCategories = async() => {
    try {
      const response = await fetch(apiruta + 'Category/getCategories' + `?token=${stateLogin.token}`);
      const responseJson = await response.json();
      setCategories(responseJson.data)

    } catch(error) {
      message.error(error, 5);
    }
  }

  const onHandleChangeImage = info => {
    if (info.file.status === 'uploading') {
      setLoading(true);
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => {
        setImageUrl(imageUrl)
        setLoading(false)
      });
    }
  };

  const onFinish = async values => {
    if(imageUrl) {
      values.image = imageUrl;
      values.token = stateLogin.token;
      try {
        const formulario = createFormData(values)
        const response = await fetch(apiruta + 'Product/createProduct', {
          method: 'post',
          body: formulario
        });
        const responseJson = await response.json();
        if (responseJson.status === 1) {
          message.success(responseJson.data, 5);
          
        }
        else {
          message.error(responseJson.data, 5);
          alert(responseJson.data)
        }
      } catch (error) {
        message.error('error en el servidor', 5);
      }
    }
  };

  const onFinishFailed = errorInfo => {
    message.error(errorInfo, 5);
  };
  return (
    <CrearProductoLayout 
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      onHandleChangeImage={onHandleChangeImage}
      loading={loading}
      imageUrl={imageUrl}
      beforeUpload={beforeUpload}
      categories={categories}
    />
  )
}
