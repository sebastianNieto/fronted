import React from 'react';
import Context from '../../context/Context';
import { message } from 'antd';
import {apiruta} from '../../utilities/constants';
import ListarProductosLayout from '../../components/product/ListarProductos';

export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);
  const [products, setProducts] = React.useState([]);

  React.useEffect(() => {
    getProducts();
  }, []);

  const getProducts = async (apiruta) => {
    try {
      const response = await fetch(apiruta + 'Product/getProducts' + `?token=${stateLogin.token}`);
      const responseJson = await response.json();
      console.log(responseJson.data)
      setProducts(responseJson.data)
    } catch (error) {
      message.error(error, 5);
    }
  }

  return (
    <ListarProductosLayout 
      products={products}
    />
  )
}
