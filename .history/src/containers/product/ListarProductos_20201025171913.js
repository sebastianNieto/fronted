import React from 'react';
import Context from '../../context/Context';
import { message } from 'antd';
import {apiruta} from '../../utilities/constants';
import ListarProductosLayout from '../../components/product/ListarProductos';

export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);
  const [products, setProducts] = React.useState([]);

  React.useEffect(() => {
    console.log('acttt')
    getProducts();
  }, []);

  const getProducts = async () => {
    try {
      const response = await fetch(apiruta + 'Product/getProducts' + `?token=${stateLogin.token}`);
      const responseJson = await response.json();
      if(responseJson.status === 1) {
        setProducts(responseJson.data)
      }
      else {
        message.error(responseJson.data, 5);
      }
      
    } catch (error) {
      console.log(error)
      message.error("error en el servidor", 5);
    }
  }

  return (
    <ListarProductosLayout 
      products={products}
    />
  )
}
