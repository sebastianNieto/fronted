import React from 'react';
import Context from '../../context/Context';
import { message, notification } from 'antd';
import {apiruta} from '../../utilities/constants';
import ListarProductosLayout from '../../components/product/ListarProductos';

export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);
  const [products, setProducts] = React.useState([]);

  React.useEffect(() => {
    getProducts();
  }, []);

  const getProducts = async () => {
    try {
      const response = await fetch(apiruta + 'Product/getProducts' + `?token=${stateLogin.token}`);
      const responseJson = await response.json();
      if(responseJson.status === 1) {
        setProducts(responseJson.data)
      }
      else {
        console.log(responseJson.data)
        notification.open({
          message: 'Notification Title',
          description:
            'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
        });
      }
      
    } catch (error) {
      alert(error)
    }
  }

  return (
    <ListarProductosLayout 
      products={products}
    />
  )
}
