import React from 'react';
import Context from '../../context/Context';
import { message, notification } from 'antd';
import {apiruta} from '../../utilities/constants';
import {createFormData} from '../../utilities/functions';
import ListarProductosLayout from '../../components/product/ListarProductos';
import 'antd/dist/antd.css';


export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);
  const [products, setProducts] = React.useState([]);

  React.useEffect(() => {
    getProducts();
  }, []);

  const getProducts = async () => {
    try {
      const response = await fetch(apiruta + 'Product/getProducts' + `?token=${stateLogin.token}`);
      const responseJson = await response.json();
      if(responseJson.status === 1) {
        setProducts(responseJson.data)
      }
      else {
        message.error(responseJson.data, 5);
      }
      
    } catch (error) {
      alert(error)
    }
  }

  const onDelete = async (event, id) => {
    event.preventDefault();
    try {
      const formulario = createFormData({id});
      const response = await fetch(apiruta + 'Product/getProducts' + `?token=${stateLogin.token}`, {
        method: 'delete',
        body: formulario
      });
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        getProducts();
      }
      else {
        message.error(responseJson.data, 5);
      }

    } catch (error) {
      alert(error)
    }
  }

  return (
    <ListarProductosLayout 
      products={products}
      onDelete={onDelete}
    />
  )
}
