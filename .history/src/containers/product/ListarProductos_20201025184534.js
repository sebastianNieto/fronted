import React from 'react';
import Context from '../../context/Context';
import { message, notification } from 'antd';
import {apiruta} from '../../utilities/constants';
import {createFormData} from '../../utilities/functions';
import ListarProductosLayout from '../../components/product/ListarProductos';
import 'antd/dist/antd.css';


export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);
  const [products, setProducts] = React.useState([]);
  const [modalText, setModalText] = React.useState('Content of the modal');
  const [visible, setVisible] = React.useState(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [productInfo, setProductInfo] = React.useState({
    image: '',
    name: '',
    description: '',
    price: 0
  });

  React.useEffect(() => {
    getProducts();
  }, []);

  const getProducts = async () => {
    try {
      const response = await fetch(apiruta + 'Product/getProducts' + `?token=${stateLogin.token}`);
      const responseJson = await response.json();
      if(responseJson.status === 1) {
        setProducts(responseJson.data)
      }
      else {
        message.error(responseJson.data, 5);
      }
      
    } catch (error) {
      alert(error)
    }
  }

  const onDelete = async (event, id) => {
    event.preventDefault();
    try {
      const formulario = createFormData({ id, token: stateLogin.token});
      const response = await fetch(apiruta + 'Product/deleteProduct', {
        method: 'post',
        body: formulario
      });
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        getProducts();
      }
      else {
        message.error(responseJson.data, 5);
      }

    } catch (error) {
      alert(error)
    }
  }

  const showModal = async(event, id) => {
    try {
      const response = await fetch(apiruta + 'Product/getProduct' + `?token=${stateLogin.token}&id=${id}`);
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        setProductInfo(responseJson.data)
      }
      else {
        message.error(responseJson.data, 5);
      }

    } catch (error) {
      alert(error)
    }
    setVisible(true)
  };

  const handleOk = () => {
    setModalText('The modal will be closed after two seconds')
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false)
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    setVisible(false)
  };

  return (
    <ListarProductosLayout
      modalText={modalText}
      visible={visible}
      products={products}
      productInfo={productInfo}
      onDelete={onDelete}
      showModal={showModal}
      handleOk={handleOk}
      handleCancel={handleCancel}
    />
  )
}
