export const initialLoguinState = {
    user: '',
    name: '',
    token: '',
    isLogin: false,
};

export const login = (state, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {
                ...state,
                ...action.payload,
                isLogin: true,
            };
        case 'LOGOUT':
            return {
                ...state,
                isLogin: false,
                name: '',
            };
        default:
            return state;
    }
};