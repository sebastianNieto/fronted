import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Context from './context/Context';
import { Router } from "@reach/router"
import Home from './pages/Home';
import Login from './pages/Login';

ReactDOM.render(
  <Context.Provider>
    <Router>
      <Home path="/" />
    <Login path="/login" />
  </Router>
  </Context.Provider>,
  document.getElementById('root')
);