import React from 'react';
import ReactDOM from 'react-dom';
import Context from './context/Context';
import { Router } from "@reach/router"
import Home from './pages/Home';
import Login from './pages/Login';
import "antd/dist/antd.css";


ReactDOM.render(
  
    <Router>
      <Home path="/" />
    <Login path="/login" />
  </Router>,
  
  document.getElementById('root')
);