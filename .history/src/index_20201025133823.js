import React from 'react';
import ReactDOM from 'react-dom';
import Context from './context/Context';
import { Router } from "@reach/router"
import Layout from './components/layout/Layout';
import ListarProductos from './pages/ListarProductos';
import Login from './pages/Login';
import "antd/dist/antd.css";


ReactDOM.render(
  <Context.Provider>
    <Router>
      <Layout path="/" >
        <ListarProductos path="/" />
      </Layout>
      <Login path="/login" />
    </Router>
  </Context.Provider>,
  document.getElementById('root')
);