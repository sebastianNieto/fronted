import React from 'react';
import ReactDOM from 'react-dom';
import Context from './context/Context';
import { Router } from "@reach/router"
import Layout from './components/layout/Layout';
import ListarProductos from './pages/ListarProductos';
import CrearProducto from './pages/CrearProducto';
import Login from './pages/Login';
import reportWebVitals from './reportWebVitals';
import 'antd/dist/antd.css';


ReactDOM.render(
  <Context.Provider>
    <Router>
      <Layout path="/" >
        <ListarProductos path="/" />
        <CrearProducto path="/crearProducto" />
      </Layout>
      <Login path="/login" />
    </Router>
  </Context.Provider>,
  document.getElementById('root')
);

reportWebVitals();