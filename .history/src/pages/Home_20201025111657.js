import React from 'react';
import Context from '../context/Context';
import { redirectTo } from "@reach/router"

export default function Home() {
  const { stateLogin } = React.useContext(Context.Context);
  if(!stateLogin.isLogin) {
    redirectTo("/login")
  }
  return (
    <div>
      Home
    </div>
  )
}
