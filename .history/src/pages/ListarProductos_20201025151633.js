import React from 'react';
import Context from '../context/Context';
import { redirectTo, navigate, Redirect } from "@reach/router"

export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);
  if(!stateLogin.isLogin) {
    return <Redirect to="/login" />
    navigate("/login")
  }
  return (
    <div>
      Home
    </div>
  )
}
