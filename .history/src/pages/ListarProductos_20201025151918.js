import React from 'react';
import Context from '../context/Context';
import { redirectTo } from "@reach/router"

export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);

  React.useEffect(() => {
    if (!stateLogin.isLogin) {
      redirectTo("/login")
    }
  }, [])
  return (
    <div>
      Home
    </div>
  )
}
