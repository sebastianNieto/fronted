import React from 'react';
import Context from '../context/Context';
import { redirectTo, navigate } from "@reach/router"

export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);

  React.useEffect(() => {
    if (!stateLogin.isLogin) {
      navigate("/login")
    }
  }, [])
  return (
    <div>
      Home
    </div>
  )
}
