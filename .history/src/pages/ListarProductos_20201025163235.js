import React from 'react';
import Context from '../context/Context';
import { navigate } from "@reach/router"
import ListarProductosContainer from '../containers/product/ListarProductos';

export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);

  React.useEffect(() => {
    if (!stateLogin.isLogin) {
      navigate("/login")
    }
  }, [])
  return (
    <ListarProductosContainer />
  )
}
