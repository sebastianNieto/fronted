export const createFormData = (data) => {
    const formulario = new FormData();
    Object.keys(data).forEach(key => {
        formulario.append(key, data[key]);
    });
    return formulario;
}