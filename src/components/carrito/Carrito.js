import React from 'react'
import { Table, Typography, Alert, Input } from 'antd';
import { CloseOutlined } from '@ant-design/icons';
import PopConfirm from '../popConfirm';
import { convertMoney } from '../../utils/functions';
import './styles.css'


export default function Carrito({ loading, carrito, onDelete, changeCantCart}) {
  const columns = [
    {
      title: 'Producto',
      dataIndex: 'producto',
    },
    {
      title: 'Cantidad',
      dataIndex: 'cantidad',
      render: (text, record) => <Input defaultValue={text} onChange={(event) => changeCantCart(event, record)}/>
    },
    {
      title: 'Precio',
      dataIndex: 'precio',
      render: text => convertMoney(text)
    },
    {
      title: 'Subtotal',
      dataIndex: 'subtotal',
      render: text => convertMoney(text)
    },
    {
      title: '',
      dataIndex: 'id',
      render: text => <PopConfirm title="¿Está seguro?" text="X" action={() => onDelete(text)}><CloseOutlined /></PopConfirm>,
    },
  ]
  const { Title } = Typography;
  let total = 0;
  if(carrito.length > 0) {
    carrito.map((item) => {
      total += parseFloat(item.subtotal)
    });
  }
  return (
    <div className="container-table-carrito ">
      <Title type="primary" style={{margin: '20px 0'}} level={2}>Carrito</Title>
      <Table
        columns={columns}
        dataSource={carrito}
      />
      <Alert message={`Total: ${convertMoney(total)}`}type="success" />
    </div>
  );
}
