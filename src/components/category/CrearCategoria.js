import React from 'react'
import { Form, Input, Button, Typography } from 'antd';
import './styles.css'

const layout = {
  labelCol: { offset: 8, span: 8 },
  wrapperCol: { offset: 8, span: 8 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 8 },
};


export default function CrearCategoria({onFinish, onFinishFailed, loading}) {
  const { Title } = Typography;
  return (
    <div className="container-categoria ">
      <Title type="primary" style={{margin: '20px 0'}} level={2}>Crear una nueva categoría</Title>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        layout="vertical"
      >
        <Form.Item
          label="Nombre"
          name="nombre"
          rules={[{ required: true, message: 'Falta ingresar el nombre de la categoría' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Descripción"
          name="descripcion"
          rules={[{ required: true, message: 'Falta ingresar la descripción de la categoría' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Crear
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
