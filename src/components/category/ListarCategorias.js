import React from 'react'
import { Table, Typography } from 'antd';
import { CloseOutlined } from '@ant-design/icons';
import PopConfirm from '../popConfirm';
import './styles.css'


export default function ListarCategorias({loading, categories, onDelete}) {
  const columns = [
    {
      title: 'Nombre',
      dataIndex: 'name',
    },
    {
      title: 'Descripción',
      dataIndex: 'description',
    },
    {
      title: 'Fecha Creación',
      dataIndex: 'date_creation',
    },
    {
      title: '',
      dataIndex: 'id',
      render: text => <PopConfirm title="¿Está seguro?" text="X" action={() => onDelete(text)}><CloseOutlined /></PopConfirm>,
    },
  ]
  const { Title } = Typography;
  console.log(categories);
  // let data = {};
  // if(categories.length) {
  //   data = categories.map(category => {
  //     return {

  //     }
  //   });
  // }
  return (
    <div className="container-table-categoria ">
      <Title type="primary" style={{margin: '20px 0'}} level={2}>Listado de categorías</Title>
      <Table
        columns={columns}
        dataSource={categories}
      />
    </div>
  );
}
