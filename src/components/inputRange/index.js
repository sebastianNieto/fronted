import React from 'react';
import { Slider, InputNumber, Row, Col } from 'antd';
import './styles.css'

const InputRange = ({onChangeValue}) => {
  const [inputValue, setInputValue] = React.useState(1)

  const onChange = value => {
    setInputValue(value);
    onChangeValue(value)
  };

  return (
    <Row className="input-range">
      <Col span={12}>
        <Slider
          min={1}
          max={20}
          onChange={onChange}
          value={typeof inputValue === 'number' ? inputValue : 0}
        />
      </Col>
      <Col span={4}>
        <InputNumber
          min={1}
          max={20}
          style={{ margin: '0 16px' }}
          value={inputValue}
          onChange={onChange}
          name="inputRange"
        />
      </Col>
    </Row>
  );
}

export default InputRange;