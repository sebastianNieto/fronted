import React from 'react';
import { Popconfirm, Button } from 'antd';

export default ({title, text, action}) => {
  const [visible, setVisible] = React.useState(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);

  const showPopconfirm = () => {
    setVisible(true);
  };

  const handleOk = async() => {
    setConfirmLoading(true);
    await action();
    setVisible(false);
    setConfirmLoading(false);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };

  return (
    <>
      <Popconfirm
        title={title}
        visible={visible}
        onConfirm={handleOk}
        okButtonProps={{ loading: confirmLoading }}
        onCancel={handleCancel}
        okText="Si"
        cancelText="No"
      >
        <a onClick={showPopconfirm}>
          {text}
        </a>
      </Popconfirm>
    </>
  );
};