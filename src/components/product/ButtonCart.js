import React from 'react';
import { Button} from 'antd';
import InputRange from '../inputRange';

export default function ButtonCart({ product, AddCart, changeInputRange}) {
  return (
    <>
      <InputRange onChangeValue={(value) => changeInputRange(value, product)} />
      <Button type="primary" htmlType="submit" onClick={() => AddCart(product)}>Agregar al carrito</Button>
    </>
  )
}
