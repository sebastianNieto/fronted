import React from 'react';
import { navigate } from "@reach/router"
import { Card, Alert, Button } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import ButtonCart from './ButtonCart';
import PopConfirm from '../popConfirm';
import {convertMoney} from '../../utils/functions';
import {apiruta} from '../../utilities/constants';

export default function CardProduct({ product, AddCart, onDelete, showModal, changeInputRange}) {
  const ButtonEdit = () => <EditOutlined style={{ color: '#096dd9', margin: '0 5px' }} onClick={() => navigate(`/crearProducto/${product.id}`)}/>;
  return (
    <Card title={`${product.name}`} extra={<><ButtonEdit /><PopConfirm title="¿Está seguro?" text="X" action={() => onDelete(product.id)} /></>} style={{ width: 300, textAlign: 'center' }}>
      <img className="image-card" src={apiruta + `/uploads/products/${product.image}`} alt="" onClick={(e) => showModal(e, product.id)} />
      <Alert className="alert-precio" message={`Precio: ${convertMoney(product.price)}`} type="info" />
      <ButtonCart 
        product={product.id}
        AddCart={AddCart}
        changeInputRange={changeInputRange}
      />
    </Card>

  )
}
