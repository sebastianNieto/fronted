import React from 'react';
import { Typography, Input, Select, Empty } from 'antd';
import CardProduct from './CardProduct';
import ModalProduct from './ModalProduct';

const { Search } = Input;
const { Option } = Select;
const { Text} = Typography;


export default function ListarProductos({ products, AddCart, onDelete, handleCancel, loading, visible, refSearch, categories, showModal, onGetProducts, onGetProductsForCategory, productInfo, changeInputRange}) {
  return (
    <>
      <div className="container-search">
        <Search placeholder="Buscar articulo" loading={loading} ref={refSearch} enterButton onSearch={onGetProducts} />
        <div style={{ marginTop: '20px' }}>
          <Text type="primary">Filtrar por categoría</Text>
          <Select style={{ width: '100%' }} onChange={onGetProductsForCategory} >
            {
              categories.length > 0 &&
              categories.map((category) => {
                return (
                  <Option key={category.id} value={category.id}>{category.name}</Option>
                )
              })
            }
          </Select>
        </div>
      </div>
      <div className="container-card" style={{marginTop: '20px'}}>
        {
          products.length > 0 ?
          products.map((product) => {
            return (
              <CardProduct 
                product={product}
                AddCart={AddCart}
                onDelete={onDelete}
                showModal={showModal}
                changeInputRange={changeInputRange}
              />
            )
          })
          : <Empty />
        }
      </div>
      <div>
        <ModalProduct 
          handleCancel={handleCancel}
          visible={visible}
          productInfo={productInfo}
        />
      </div>
    </>
  )
}
