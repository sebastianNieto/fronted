import React from 'react';
import { Modal } from 'antd';
import { Typography } from 'antd';
import { convertMoney } from '../../utils/functions';
import {apiruta} from '../../utilities/constants';

const { Title, Text } = Typography;

export default function ModalProducto({ handleCancel, visible, productInfo}) {
  return (
    <Modal
      title={`${productInfo.name}`}
      visible={visible}
      onOk={handleCancel}
      onCancel={handleCancel}
      cancelText="Cancelar"
      style={{textAlign: "center"}}
    >
      <img className="image-card image-card-modal" src={apiruta + `/uploads/products/${productInfo.image}`} alt={`${productInfo.name}`}/>
      <Title level={3} style={{marginTop: '10px'}}>{`${productInfo.name}`}</Title>
      <div>{`${productInfo.description}`}</div>
      <div>Precio: <Text mark>{`${convertMoney(productInfo.price)}`}</Text></div>
      <div>{`${productInfo.date_creation}`}</div>
    </Modal>
  )
}
