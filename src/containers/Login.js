import React from 'react';
import Context from '../context/Context';
import LoginLayout from '../components/login/index';
import { navigate } from "@reach/router"
import { message } from 'antd';
import { apiruta } from '../utilities/constants';
import { createFormData } from '../utilities/functions';

export default function Login() {
    const { dispatchLogin } = React.useContext(Context.Context);
    const [loading, setLoading] = React.useState(false);
    const onFinish = async values => {
        setLoading(true);
        try {
            const formulario = createFormData(values)
            const response = await fetch(apiruta + 'Login/login', {
                method: 'post',
                body: formulario
            });
            const responseJson = await response.json();
            if (responseJson.status === 1) {
                dispatchLogin({
                    type: 'LOGIN',
                    payload: {
                        user: responseJson.data.dataUser.user,
                        name: responseJson.data.dataUser.name,
                        token: responseJson.data.token,
                        isLogin: true
                    },
                })
                navigate('/')
            } else {
                message.error(responseJson.data);
            }
            setLoading(false);
        } catch (error) {
            message.error('Error en el servidor');
            setLoading(false);
        }
    };

    const onFinishFailed = errorInfo => {
        console.error(errorInfo);
        setLoading(false);
    };

    return ( <LoginLayout onFinish = { onFinish } onFinishFailed = { onFinishFailed } loading={loading} />
    )
}