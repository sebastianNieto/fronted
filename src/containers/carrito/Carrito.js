import React from 'react';
import Context from '../../context/Context';
import { message } from 'antd';
import { apiruta } from '../../utilities/constants';
import { createFormData } from '../../utilities/functions';
import CarritoLayout from '../../components/carrito/Carrito';


export default function Carrito() {
  const { stateLogin } = React.useContext(Context.Context);
  const [loading, setLoading]  = React.useState();
  const [carrito, setCarrito] = React.useState([]);

  React.useEffect(() => {
    getCarrito()
  }, []);

  const getCarrito = async() => {
    try {
      const response = await fetch(apiruta + 'Carrito/getCarrito' + `?token=${stateLogin.token}`);
      const responseJson = await response.json();
      setCarrito(responseJson.data)

    } catch(error) {
      message.error('Error en el servidor');
    }
  }

  const onDelete = async (id) => {
    try {
      const formulario = createFormData({ id, token: stateLogin.token });
      const response = await fetch(apiruta + 'Carrito/deleteCarrito', {
        method: 'post',
        body: formulario
      });
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        getCarrito();
      }
      else {
        message.error(responseJson.data, 5);
      }

    } catch (error) {
      message.error('Error en el servidor');
    }
  }

  const changeCantCart = async (event, row) => {
    const value = event?.nativeEvent?.data;
    if(value !== null && value > 0) {
      try {
        const formulario = createFormData({ producto: row.id, cantidad: value,token: stateLogin.token });
        const response = await fetch(apiruta + 'Carrito/updateCarrito', {
          method: 'post',
          body: formulario
        });
        const responseJson = await response.json();
        if (responseJson.status === 1) {
          getCarrito();
          message.success(responseJson.data);
        }
        else {
          message.error(responseJson.data, 5);
        }

      } catch (error) {
        message.error('Error en el servidor');
      }
    }
  }

  return (
    <CarritoLayout
      loading={loading}
      carrito={carrito}
      onDelete={onDelete}
      changeCantCart={changeCantCart}
    />
  )
}
