import React from 'react';
import Context from '../../context/Context';
import { message } from 'antd';
import { apiruta } from '../../utilities/constants';
import { createFormData } from '../../utilities/functions';
import CrearCategoriaLayout from '../../components/category/CrearCategoria';


export default function CrearCategoria() {
  const { stateLogin } = React.useContext(Context.Context);
  const [loading, setLoading]  = React.useState();

  const onFinish = async values => {
    values.token = stateLogin.token;
    try {
      const formulario = createFormData(values)
      const response = await fetch(apiruta + 'Category/createCategory', {
        method: 'post',
        body: formulario
      });
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        message.success(responseJson.data, 5);
        
      }
      else {
        message.error(responseJson.data, 5);
      }
    } catch (error) {
      message.error('Error en el servidor', 5);
    }
  };

  const onFinishFailed = errorInfo => {
    console.error(errorInfo, 5);
  };

  return (
    <CrearCategoriaLayout 
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      loading={loading}
    />
  )
}
