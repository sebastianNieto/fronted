import React from 'react';
import Context from '../../context/Context';
import { message } from 'antd';
import { apiruta } from '../../utilities/constants';
import { createFormData } from '../../utilities/functions';
import ListarCategoriasLayout from '../../components/category/ListarCategorias';


export default function ListarCategorias() {
  const { stateLogin } = React.useContext(Context.Context);
  const [loading, setLoading]  = React.useState();
  const [categories, setCategories] = React.useState([]);

  React.useEffect(() => {
    getCategories()
  }, []);

  const getCategories = async() => {
    try {
      const response = await fetch(apiruta + 'Category/getCategories' + `?token=${stateLogin.token}`);
      const responseJson = await response.json();
      setCategories(responseJson.data)

    } catch(error) {
      message.error('Error en el servidor');
    }
  }

  const onDelete = async (id) => {
    try {
      const formulario = createFormData({ id, token: stateLogin.token });
      const response = await fetch(apiruta + 'Category/deleteCategory', {
        method: 'post',
        body: formulario
      });
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        getCategories();
      }
      else {
        message.error(responseJson.data, 5);
      }

    } catch (error) {
      message.error('Error en el servidor');
    }
  }

  return (
    <ListarCategoriasLayout
      loading={loading}
      categories={categories}
      onDelete={onDelete}
    />
  )
}
