import React from 'react';
import Context from '../../context/Context';
import { message } from 'antd';
import {apiruta} from '../../utilities/constants';
import {createFormData} from '../../utilities/functions';
import ListarProductosLayout from '../../components/product/ListarProductos';
import 'antd/dist/antd.css';


export default function ListarProductos() {
  const { stateLogin } = React.useContext(Context.Context);
  const [products, setProducts] = React.useState([]);
  const [categories, setCategories] = React.useState([]);
  const [modalText, setModalText] = React.useState('Content of the modal');
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [cantCart, setCantCart] = React.useState({
    product: 0,
    cant: 0
  });
  const [productInfo, setProductInfo] = React.useState({
    image: '',
    name: '',
    description: '',
    price: 0
  });

  const refSearch = React.useRef();

  React.useEffect(() => {
    getProducts();
    getCategories();
  }, []);

  const getProducts = async () => {
    setLoading(true);
    try {
      const param = refSearch?.current?.state?.value ?? '';
      const response = await fetch(apiruta + 'Product/getProducts' + `?token=${stateLogin.token}&param=${param}`);
      const responseJson = await response.json();
      if(responseJson.status === 1) {
        setProducts(responseJson.data)
      }
      else {
        message.error(responseJson.data, 5);
      }
      setLoading(false);
      
    } catch (error) {
      message.error('Error en el servidor');
      setLoading(false);
    }
  }

  const getProductsForCategory = async (category) => {
    try {
      const response = await fetch(apiruta + `Product/getProducts/${category}?token=${stateLogin.token}`);
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        setProducts(responseJson.data)
      }
      else {
        message.error(responseJson.data, 5);
      }
      setLoading(false);

    } catch (error) {
      message.error('Error en el servidor');
      setLoading(false);
    }
  }

  const getCategories = async () => {
    try {
      const response = await fetch(apiruta + 'Category/getCategories' + `?token=${stateLogin.token}`);
      const responseJson = await response.json();
      setCategories(responseJson.data)

    } catch (error) {
      message.error('Error en el servidor');
    }
  }

  const onDelete = async (id) => {
    try {
      const formulario = createFormData({ id, token: stateLogin.token});
      const response = await fetch(apiruta + 'Product/deleteProduct', {
        method: 'post',
        body: formulario
      });
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        getProducts();
      }
      else {
        message.error(responseJson.data, 5);
      }

    } catch (error) {
      message.error('Error en el servidor');
    }
  }

  const showModal = async(event, id) => {
    try {
      const response = await fetch(apiruta + 'Product/getProduct' + `?token=${stateLogin.token}&id=${id}`);
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        setProductInfo(responseJson.data, setVisible(true))

      }
      else {
        message.error(responseJson.data, 5);
      }

    } catch (error) {
      message.error('Error en el servidor');
    }
  };

  const handleCancel = () => {
    setVisible(false)
  };

  const changeInputRange = (value, product) => {
    setCantCart({
      product,
      cant: value
    });
  }

  const AddCart = async(product) => {
    try {
      let data = {}
      if(product === cantCart.product) {
        data = cantCart;
      }
      else {
        data = { product, cant: 1}
      }
      data.token = stateLogin.token;
      const formulario = createFormData(data)
      const response = await fetch(apiruta + 'Carrito/ingresar', {
        method: 'post',
        body: formulario
      });
      const responseJson = await response.json();
      if (responseJson.status === 1) {
        message.success(responseJson.data);
      }
      else {
        message.error(responseJson.data, 5);
      }

    } catch (error) {
      message.error('Error en el servidor');
    }
  };


  return (
    <ListarProductosLayout
      modalText={modalText}
      visible={visible}
      products={products}
      productInfo={productInfo}
      refSearch={refSearch}
      loading={loading}
      categories={categories}
      onGetProducts={getProducts}
      onGetProductsForCategory={getProductsForCategory}
      AddCart={AddCart}
      onDelete={onDelete}
      showModal={showModal}
      handleCancel={handleCancel}
      changeInputRange={changeInputRange}
    />
  )
}
