import React from 'react'
import { login, initialLoguinState } from './states/login';

const Context = React.createContext({})

const Provider = ({ children }) => {
  const [stateLogin, dispatchLogin] = React.useReducer(login, initialLoguinState);
  const loginReduce = { stateLogin, dispatchLogin }

  return (
    <Context.Provider value={loginReduce}>
      {children}
    </Context.Provider>
  )
}

export default {
  Provider,
  Context
}
