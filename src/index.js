import React from 'react';
import ReactDOM from 'react-dom';
import Context from './context/Context';
import { Router } from "@reach/router"
import Layout from './components/layout/Layout';
import ListarProductos from './pages/ListarProductos';
import CrearProducto from './pages/CrearProducto';
import ListarCategorias from './pages/ListarCategorias';
import CrearCategoria from './pages/CrearCategoria';
import Carrito from './pages/Carrito';
import Login from './pages/Login';
import reportWebVitals from './reportWebVitals';
import 'antd/dist/antd.css';


ReactDOM.render(
  <Context.Provider>
    <Router>
      <Layout path="/" >
        <ListarProductos path="/" />
        <CrearProducto path="/crearProducto" />
        <ListarCategorias path="/ListarCategorias" />
        <CrearCategoria path="/crearCategoria" />
        <Carrito path="/carrito" />
      </Layout>
      <Login path="/login" />
    </Router>
  </Context.Provider>,
  document.getElementById('root')
);

reportWebVitals();