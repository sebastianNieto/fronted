import React from 'react';
import Context from '../context/Context';
import { navigate } from "@reach/router"
import CarritoContainer from '../containers/carrito/Carrito';

export default function ListarProductos() {
    const { stateLogin } = React.useContext(Context.Context);

    React.useEffect(() => {
        if (!stateLogin.isLogin) {
            navigate("/login")
        }
    }, [])

  return stateLogin.isLogin ? < CarritoContainer /> : null
}